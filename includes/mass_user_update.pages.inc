<?php

/**
 * @file
 * Defines page and form callbacks.
 */

/**
 * Mass user update form, first step
 * Load a file and extract the labels. Prepare for the second step.
 */
function mass_user_update_update_form() {
  $form = array(
    '#attributes' => array('enctype' => 'multipart/form-data')
  );

  $form['mass_user_update_file'] = array(
    '#type' => 'file',
    '#title' => t("Select file to import"),
    '#default_value' => 0,
    '#description' => t("Allowed extensions: @formats", array('@formats' => implode(', ', mass_user_update_allowed_formats()))),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Import file"),
  );

  return $form;
}

/**
 * Step 1 validation
 */
function mass_user_update_update_form_validate($form, &$form_state) {
 if (isset($_FILES['files']) && is_uploaded_file($_FILES['files']['tmp_name']['mass_user_update_file'])) {
    $file = file_save_upload('mass_user_update_file');

    if (!$file) {
      form_set_error('mass_user_update_file', t("Error saving uploaded file."));
      return;
    }
    else {
      if (!in_array(strtolower(array_pop(explode('.', $file->filename)), mass_user_update_allowed_formats(), TRUE) {
        form_set_error('mass_user_update_file', t("Only files in the following formats are allowed: @formats", array('@formats' => implode(', ', mass_user_update_allowed_formats()))));
      }
      else {
        $form_state['file'] = $file;
      }
    }
  }
  else {
    form_set_error('i18n_importexport_file', t("Error uploading file."));
    return;
  }
}

/**
 * Step 1 submission
 */
function mass_user_update_update_form_submit($form, $form_state) {
  drupal_goto("admin/user/user/mass-update/{$form_state['file']->filename}");
}

/**
 * Mass user update form, second step.
 * Allow admins to mapp columns to user fields.
 */
function mass_user_update_update_map_form() {
  dpm(func_get_args());
}
