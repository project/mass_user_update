<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */


/**
 * @defgroup mass_user_update Mass User Update module integrations.
 *
 * Module integrations with the Mass User Update module.
 */

/**
 * @defgroup mass_user_update_hooks Mass User Update's hooks
 * @{
 * Hooks that can be implemented by other modules in order to extend Mass User
 * Update.
 */

/**
 *
 */
function hook_rules_action_info() {
  return array(
    'mail_user' => array(
      'label' => t('Send a mail to a user'),
      'parameter' => array(
        'user' => array('type' => 'user', 'label' => t('Recipient')),
      ),
      'group' => t('System'),
      'base' => 'rules_action_mail_user',
      'callbacks' => array(
        'validate' => 'rules_action_custom_validation',
        'help' => 'rules_mail_help',
      ),
    ),
  );
}


/**
 * @}
 */
